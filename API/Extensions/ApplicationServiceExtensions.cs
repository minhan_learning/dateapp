using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data;
using API.Interface;
using API.Service;
using Microsoft.EntityFrameworkCore;

namespace API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        //Đăng ký lớp DataContext (lớp kết nối cơ sở dữ liệu) 
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration
        config)
        {
            services.AddDbContext<DataContext>(opt =>
        {
            opt.UseSqlite(config.GetConnectionString("defaultConnection"));
        });

            services.AddCors();

            //sử dụng để đăng ký dịch vụ TokenService với giao diện ITokenService trong container
            services.AddScoped<ITokenService, TokenService>();
            return services;
        }
    }
}